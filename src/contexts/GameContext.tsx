import React, { createContext, useContext } from "react";
import { createGameGrid } from "../utils/Utils";

export const GameContext = createContext<
  {
      board: GameGrid;
      setBoard: React.Dispatch<React.SetStateAction<GameGrid>>;
      gameState: GameState;
      setGameState: React.Dispatch<React.SetStateAction<GameState>>;
      gameDifficulty: number;
      setGameDifficulty: React.Dispatch<React.SetStateAction<number>>;
    } 
>({gameDifficulty: 15, setGameDifficulty: ()=>{}, board:createGameGrid(15, 15, 15, "🐰"), setBoard:()=>{}, gameState: 'new', setGameState: ()=>{}});

export function useGameContext() {
  const context = useContext(GameContext);
  if (!context) {
    throw new Error("useGameContext must be used within a GameProvider");
  }
  return context;
}
