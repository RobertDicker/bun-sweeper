import { useContext, useEffect, useState } from "react";
import "./Timer.css";
import { GameContext } from "../../contexts/GameContext";

export default function Timer() {
  const [seconds, setSeconds] = useState(0);
  const { gameState } = useContext(GameContext);

  useEffect(() => {
    if (gameState !== "won" && gameState !== "lost") {
      const id = setInterval(() => {
        setSeconds((s) => s + 1);
      }, 1000);
      if (gameState === "new") {
        setSeconds(0);
      }

      return () => {
        clearInterval(id);
      };
    }
  }, [gameState]);

  return <span className={"timer"}>⏰ {seconds}</span>;
}
