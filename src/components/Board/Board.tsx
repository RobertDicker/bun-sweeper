import { useContext } from "react";

import { GAME_ICON } from "../../constants/GameConstants";

import { GameContext } from "../../contexts/GameContext";
import { copyBoard, revealTiles } from "../../utils/Utils";
import Cell from "../Cell/Cell";

export default function Board() {
  const {
    board,
    setBoard: updateBoard,
    setGameState,
  } = useContext(GameContext);

  const handleCellClick = (row: number, col: number) => {
    const currentCell = board[row][col];
    if (currentCell.state === "revealed") {
      return;
    }
    if (currentCell.value === GAME_ICON) {
      setGameState("lost");
    }

    const newBoard = revealTiles(board, row, col);

    updateBoard(newBoard);
  };

  const handleRightClick = (row: number, col: number) => {
    const copiedBoard = copyBoard(board);
    const currentCell = copiedBoard[row][col];
    console.log("right click", currentCell);
    if (currentCell.state === "revealed") {
      return;
    }

    if (currentCell.state === "flagged") {
      currentCell.state = "hidden";
    } else {
      console.log("set state");
      currentCell.state = "flagged";
    }
    updateBoard(copiedBoard);
  };

  const grid = board.map((row, rowIndex) => {
    const rowCells = row.map((col, colIndex) => (
      <>
        <Cell
          key={"r" + rowIndex + "c" + colIndex}
          cellState={col.state}
          value={col.value}
          handleCellClick={() => handleCellClick(rowIndex, colIndex)}
          handleRightClick={(e) => {
            e.preventDefault();
            handleRightClick(rowIndex, colIndex);
          }}
        ></Cell>
      </>
    ));
    return <div key={rowIndex} className="row">{rowCells}</div>;
  });

  return <div id="board">{grid}</div>;
}
