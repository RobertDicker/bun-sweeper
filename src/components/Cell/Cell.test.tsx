import { fireEvent, render } from "@testing-library/react";
import {
    FLAGGED_ICON,
    HIDDEN_ICON,
    REVEALED_ICON
} from "../../constants/GameConstants";
import Cell from "./Cell";

describe("Cell component", () => {
  const mockHandleCellClick = jest.fn();
  const mockHandleRightClick = jest.fn();

  test("renders correctly with hidden state", () => {
    const { getByText } = render(
      <Cell
        cellState="hidden"
        value="1"
        handleCellClick={mockHandleCellClick}
        handleRightClick={mockHandleRightClick}
      />
    );
    const cellElement = getByText(HIDDEN_ICON);
    expect(cellElement).toBeInTheDocument();
  });

  it("renders correctly with revealed state and value 0", () => {
    const { getByText } = render(
      <Cell
        cellState="revealed"
        value="0"
        handleCellClick={mockHandleCellClick}
        handleRightClick={mockHandleRightClick}
      />
    );
    const cellElement = getByText(REVEALED_ICON);
    expect(cellElement).toBeInTheDocument();
  });

  it("renders correctly with revealed state and non-zero value", () => {
    const { getByText } = render(
      <Cell
        cellState="revealed"
        value="2"
        handleCellClick={mockHandleCellClick}
        handleRightClick={mockHandleRightClick}
      />
    );
    const cellElement = getByText("2");
    expect(cellElement).toBeInTheDocument();
  });

  it("renders correctly with flagged state", () => {
    const { getByText } = render(
      <Cell
        cellState="flagged"
        value="1"
        handleCellClick={mockHandleCellClick}
        handleRightClick={mockHandleRightClick}
      />
    );
    const cellElement = getByText(FLAGGED_ICON);
    expect(cellElement).toBeInTheDocument();
  });

  it("calls handleCellClick when clicked", () => {
    const { getByText } = render(
      <Cell
        cellState="hidden"
        value="1"
        handleCellClick={mockHandleCellClick}
        handleRightClick={mockHandleRightClick}
      />
    );
    const cellElement = getByText(HIDDEN_ICON);
    fireEvent.click(cellElement);
    expect(mockHandleCellClick).toHaveBeenCalled();
  });

  it("calls handleRightClick when right-clicked", () => {
    const { getByText } = render(
      <Cell
        cellState="hidden"
        value="1"
        handleCellClick={mockHandleCellClick}
        handleRightClick={mockHandleRightClick}
      />
    );
    const cellElement = getByText(HIDDEN_ICON);
    fireEvent.contextMenu(cellElement);
    expect(mockHandleRightClick).toHaveBeenCalled();
  });
});
