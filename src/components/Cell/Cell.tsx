import {
  FLAGGED_ICON,
  GAME_ICON,
  HIDDEN_ICON,
  REVEALED_ICON,
} from "../../constants/GameConstants";
import "./Cell.css";

interface props {
  cellState: "revealed" | "flagged" | "hidden" | undefined;
  value: string;
  handleCellClick: (e: React.MouseEvent<HTMLElement>) => void;
  handleRightClick: (e: React.MouseEvent<HTMLElement>) => void;
}

export default function Cell({
  cellState,
  value,
  handleCellClick,
  handleRightClick,
}: props) {
  const display = {
    revealed: value === "0" ? REVEALED_ICON : value,
    flagged: FLAGGED_ICON,
    hidden: HIDDEN_ICON,
  };

  const fontMap: Record<string, string> = {
    0: "zero",
    1: "one",
    2: "two",
    [GAME_ICON]: "target",
  };

  let classes = `cell-button ` + cellState;
  if (cellState === "revealed") {
    classes += ` ${fontMap[value] || ".danger"}`;
  }

  return (
    <span
      className={classes}
      onClick={handleCellClick}
      onContextMenu={handleRightClick}
    >
      {display[cellState || "hidden"]}
    </span>
  );
}
