import { useContext } from "react";
import { GameContext } from "../../contexts/GameContext";
import { createGameGrid } from "../../utils/Utils";
import "./NewGameButton.css";

export default function NewGameButton() {
  const { setBoard, setGameState, gameDifficulty } = useContext(GameContext);

  const  handleNewBoardClick = ()=> {
    setBoard(createGameGrid(15, 15, gameDifficulty, "🐰"));
    setGameState("new");
  };

  return (
    <>
      <button onClick={handleNewBoardClick}>Let me at 'em!</button>
    </>
  );
}
