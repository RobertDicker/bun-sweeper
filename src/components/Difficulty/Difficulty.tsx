import { useContext } from "react";
import { GameContext } from "../../contexts/GameContext";
import "./Difficulty.css";

export function Difficulty() {
  const { gameDifficulty, setGameDifficulty } = useContext(GameContext);

  const handleBunClick = () => {
    setGameDifficulty((s) => (s + 1 > 100 ? 100 : s + 1));
  };

  const handleRightClick = (
    e: React.MouseEvent<HTMLHeadingElement, MouseEvent>
  ) => {
    e.preventDefault();
    setGameDifficulty((s) => (s - 1 < 10 ? 10 : s - 1));
  };

  return (
    <div style={{ display: "flex" }}>
      <h3
        className="difficulty-bunny tooltip"
        onClick={handleBunClick}
        onContextMenu={(e) => handleRightClick(e)}
      >
        🐇<span className="tooltiptext">Will apply on the next game
        <div>(Left +1, Right -1)</div> </span>
      </h3>

      <h3 key={"difficulty" + gameDifficulty} className="difficulty-heading">
        x{gameDifficulty}
      </h3>
    </div>
  );
}
