import { useContext } from "react";
import { GameContext } from "../../contexts/GameContext";
import "./Modal.css";
export default function Modal({ children }: { children: React.ReactNode }) {
  
  const { gameState } = useContext(GameContext);
  const isDiplayed = gameState === "won" || gameState === "lost";
  const message =
    gameState === "won"
      ? "🥕Congratulations you win! Again?🥕"
      : gameState === "lost"
      ? "🌲🌲They're too fast for you and escaped 🌲🌲"
      : "";

  return (
    <dialog className="modal" open={isDiplayed}>
      <div className="modal-content">
        {children}
        <h3>{message}</h3>
      </div>
    </dialog>
  );
}
