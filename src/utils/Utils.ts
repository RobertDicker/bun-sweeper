import { GAME_ICON } from "../constants/GameConstants";

export function generateGridArray(rows: number, cols: number): GameGrid {
  let gameGrid: GameGrid = [];
  for (let r = 0; r < rows; r++) {
    gameGrid[r] = [];
    for (let c = 0; c < cols; c++) {
      gameGrid[r][c] = { value: "0", state: "hidden" };
    }
  }

  return gameGrid;
}

export function createGameGrid(
  rows: number,
  cols: number,
  numToInsert: number,
  symbol: string
) {
  if (rows * cols < numToInsert) {
    throw new Error("Cannot insert more numbers than grid cells");
  }
  // Initialize a 2D array (grid) with zeros
  const grid = generateGridArray(rows, cols);

  while (numToInsert > 0) {
    const randRow = Math.floor(Math.random() * grid.length);
    const randCol = Math.floor(Math.random() * grid[randRow].length);

    if (grid[randRow][randCol].value !== symbol) {
      grid[randRow][randCol].value = symbol;
      computeAdjacentNodes(grid, randRow, randCol); // Implement this function
      numToInsert--;
    }
  }

  return grid;
}
const isOutOfBounds = (
  row: number,
  col: number,
  height: number,
  width: number
) => row < 0 || row >= height || col < 0 || col >= width;

function updateNodeCount(grid: GameGrid, row: number, col: number) {
  if (isOutOfBounds(row, col, grid.length, grid[0].length)) {
    return;
  }

  const currentCount = grid[row][col].value;

  if (isNaN(Number(currentCount))) {
    return;
  }
  grid[row][col].value = String(Number(currentCount) + 1);
}

function computeAdjacentNodes(grid: GameGrid, row: number, col: number) {
  updateNodeCount(grid, row + 1, col);
  updateNodeCount(grid, row - 1, col);
  updateNodeCount(grid, row, col + 1);
  updateNodeCount(grid, row, col - 1);
  //Diags
  updateNodeCount(grid, row + 1, col + 1);
  updateNodeCount(grid, row - 1, col - 1);
  updateNodeCount(grid, row + 1, col - 1);
  updateNodeCount(grid, row - 1, col + 1);
}

export function copyBoard(grid: GameGrid): GameGrid {
  return grid.map((r) => r.map((c) => ({value: c.value, state: c.state})));
}

export function revealTiles(
  grid: GameGrid,
  row: number,
  col: number
): GameGrid {
  const boardCopy: GameGrid = copyBoard(grid);
  const currentCell = boardCopy[row][col];

  if (currentCell.state === "flagged") {
    boardCopy[row][col].state = "hidden";
    return boardCopy;
  }

  if (currentCell.value === GAME_ICON) {
    //Reveal the whole board the rabbits;
    for (const r of boardCopy) {
      for (const c of r) {
        c.state = "revealed";
      }
    }
  }

  if (currentCell.value === "0") {
    floodFillSurroundingZeros(boardCopy, row, col);
  } else {
    boardCopy[row][col].state = "revealed";
  }

  return boardCopy;
}

function floodFillSurroundingZeros(grid: GameGrid, row: number, col: number) {
  if (isOutOfBounds(row, col, grid.length, grid?.[0].length)) {
    return;
  }

  if (grid[row][col].value !== "0") {
    return;
  }

  if (grid[row][col].state === "revealed") {
    return;
  }

  grid[row][col].state = "revealed";

  //Surrounds
  floodFillSurroundingZeros(grid, row + 1, col);
  floodFillSurroundingZeros(grid, row - 1, col);
  floodFillSurroundingZeros(grid, row, col + 1);
  floodFillSurroundingZeros(grid, row, col - 1);
  //Diags
  floodFillSurroundingZeros(grid, row + 1, col + 1);
  floodFillSurroundingZeros(grid, row - 1, col - 1);
  floodFillSurroundingZeros(grid, row + 1, col - 1);
  floodFillSurroundingZeros(grid, row - 1, col + 1);
}

export function checkForWinCondition(grid: GameGrid): boolean {
  for (const row of grid) {
    for (const cell of row) {
      //All revealed except rabbits
      if (cell.value !== GAME_ICON && cell.state !== "revealed") {
        return false;
      }
    }
  }
  return true;
}
