import { GAME_ICON } from "../constants/GameConstants";
import {
  checkForWinCondition,
  copyBoard,
  generateGridArray,
  revealTiles,
} from "./Utils";

describe("Utils functions", () => {
  test("creates a grid array correctly with default values", () => {
    const grid = generateGridArray(4, 4);

    for (const row of grid) {
      for (const cell of row) {
        expect(cell).toStrictEqual({ value: "0", state: "hidden" });
      }
    }
  });

  test("copy board returns copy and not original ", () => {
    const grid = generateGridArray(4, 4);
    const copy = copyBoard(grid);

    copy[0][1] = { value: "copy", state: "revealed" };

    expect(grid[0][1]).toMatchObject({ value: "0", state: "hidden" });
    expect(copy[0][1]).toMatchObject({ value: "copy", state: "revealed" });
  });

  test("check for win condition returns true when all except game icon is revealed", () => {
    const grid = generateGridArray(4, 4);
    grid[0][0] = { value: GAME_ICON, state: "hidden" };

    let hasWonCondition = checkForWinCondition(grid);
    expect(hasWonCondition).toBeFalsy();
    grid.forEach((row) =>
      row.forEach((cell) => {
        cell.value !== GAME_ICON;
        cell.state = "revealed";
      })
    );
    hasWonCondition = checkForWinCondition(grid);
    expect(hasWonCondition).toBeTruthy();
  });

  test("check for win condition returns true when all revealed and game icon flagged", () => {
    const grid = generateGridArray(4, 4);
    grid[0][0] = { value: GAME_ICON, state: "flagged" };

    let hasWonCondition = checkForWinCondition(grid);
    expect(hasWonCondition).toBeFalsy();
    grid.forEach((row) =>
      row.forEach((cell) => {
        cell.value !== GAME_ICON;
        cell.state = "revealed";
      })
    );
    hasWonCondition = checkForWinCondition(grid);
    expect(hasWonCondition).toBeTruthy();
  });

  test("check win condition returns false if non game icon is flagged and rest revealed", () => {
    const grid = generateGridArray(4, 4);
    //This is the win condition it is hidden
    grid[0][0] = { value: GAME_ICON, state: "hidden" };

    let hasWonCondition = checkForWinCondition(grid);
    expect(hasWonCondition).toBeFalsy();
    grid.forEach((row) =>
      row.forEach((cell) => {
        cell.value !== GAME_ICON;
        cell.state = "revealed";
      })
    );
    grid[0][1] = { value: "0", state: "flagged" };
    hasWonCondition = checkForWinCondition(grid);
    expect(hasWonCondition).toBeFalsy();
  });

  describe("reveal tiles click zero", () => {
    let gameGrid: GameGrid;
    const revealedZeroTile = { value: "0", state: "revealed" };

    beforeAll(() => {
      const grid = generateGridArray(8, 8);

      grid[5][4] = { value: "0", state: "flagged" };
      grid[1][1] = { value: GAME_ICON, state: "hidden" };
      grid[2][2] = { value: "1", state: "hidden" };
      grid[5][5] = { value: "3", state: "flagged" };
      grid[7][7] = { value: GAME_ICON, state: "hidden" };
      grid[3][0] = { value: GAME_ICON, state: "hidden" };
      grid[1][0] = { value: GAME_ICON, state: "hidden" };
      gameGrid = revealTiles(grid, 0, 1);

      expect(grid).not.toStrictEqual(gameGrid);
    });

    test("clicked zero tile should be revealed", () => {
      expect(gameGrid[0][1]).toStrictEqual(revealedZeroTile);
    });

    test("flagged zeroes are revealed", () => {
      expect(gameGrid[5][4]).toStrictEqual(revealedZeroTile);
    });

    test("game icon is not revealed", () => {
      const hiddenGameIcons = { value: GAME_ICON, state: "hidden" };
      expect(gameGrid[1][1]).toMatchObject(hiddenGameIcons);
      expect(gameGrid[1][0]).toMatchObject(hiddenGameIcons);
      expect(gameGrid[3][0]).toMatchObject(hiddenGameIcons);
      expect(gameGrid[7][7]).toMatchObject(hiddenGameIcons);
    });

    test("numbered tiles should not be revealed", () => {
      expect(gameGrid[2][2]).toStrictEqual({ value: "1", state: "hidden" });
      expect(gameGrid[5][5]).toStrictEqual({ value: "3", state: "flagged" });
    });

    test("edge zero tiles should be revealed", () => {
      expect(gameGrid[0][0]).toStrictEqual(revealedZeroTile);
      expect(gameGrid[0][7]).toStrictEqual(revealedZeroTile);
      expect(gameGrid[7][0]).toStrictEqual(revealedZeroTile);
      expect(gameGrid[4][0]).toStrictEqual(revealedZeroTile);
      expect(gameGrid[4][7]).toStrictEqual(revealedZeroTile);
      expect(gameGrid[5][7]).toStrictEqual(revealedZeroTile);
      expect(gameGrid[5][0]).toStrictEqual(revealedZeroTile);
    });
  });
});
