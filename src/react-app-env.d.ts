/// <reference types="react-scripts" />
type GameGrid = {
    value: string;
    state: "hidden" | "revealed" | "flagged";
  }[][];

type GameState = "won" | "lost" | "playing" | "new"