import { useEffect, useState } from "react";

import Board from "../components/Board/Board";
import { Difficulty } from "../components/Difficulty/Difficulty";
import Modal from "../components/Modal/Modal";
import NewGameButton from "../components/NewGameButton/NewGameButton";
import Timer from "../components/Toolbar/Timer";
import { GameContext } from "../contexts/GameContext";
import { checkForWinCondition, createGameGrid } from "../utils/Utils";
import "./Game.css";

export default function Game() {
  const [gameDifficulty, setGameDifficulty] = useState(16);
  const [board, setBoard] = useState(() =>
    createGameGrid(15, 15, gameDifficulty, "🐰")
  );
  const [gameState, setGameState] = useState<GameState>("playing");

  useEffect(() => {
    if (gameState === "lost") {
      return;
    }
    const hasWon = checkForWinCondition(board);
    if (hasWon) {
      setGameState("won");
    } else setGameState("playing");
  }, [board, gameState]);

  return (
    <GameContext.Provider
      value={{
        board,
        setBoard,
        gameState,
        setGameState,
        gameDifficulty,
        setGameDifficulty,
      }}
    >
      <Modal>
        <NewGameButton />
      </Modal>
      <div className={"game"}>
        <div className="heading">
          <span>🐰Bun-Sweeper🥕</span>
        </div>
        <div className="game-details">
          <Difficulty />
          <Timer />
        </div>

        <Board />
      </div>
    </GameContext.Provider>
  );
}
