export const GAME_ICON = '🐰';
export const HIDDEN_ICON = '🌲';
export const REVEALED_ICON = '🌿'
export const FLAGGED_ICON = '🥕';